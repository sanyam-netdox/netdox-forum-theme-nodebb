# README #

Onco theme for NodeBB
====================

This is a fork of the current default theme for NodeBB: 
[Persona](https://github.com/NodeBB/nodebb-theme-persona)

So essentially an npm package.

### What is this repository for? ###

* Onco.com community forum theme customizations

### How do I get set up? ###

* npm install $link_to_this_repo

### Contribution guidelines ###

* Please feel free to clone, modify, and send pull requests ! 

### Who do I talk to? ###

* varun@onco.com, sanyam@onco.com
