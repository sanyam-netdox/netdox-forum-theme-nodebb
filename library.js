'use strict';

var striptags = require('striptags');
var meta = module.parent.require('./meta');
var user = module.parent.require('./user');

var _ = require('lodash');
var posts = module.parent.require('./posts');
var utils = module.parent.require('./utils');
var categories = module.parent.require('./categories');
var async = require('async');

var library = {};

library.init = function(params, callback) {
	var app = params.router;
	var middleware = params.middleware;

	app.get('/admin/plugins/persona', middleware.admin.buildHeader, renderAdmin);
	app.get('/api/admin/plugins/persona', renderAdmin);

	callback();
};

library.addAdminNavigation = function(header, callback) {
	header.plugins.push({
		route: '/plugins/persona',
		icon: 'fa-paint-brush',
		name: 'Persona Theme'
	});

	callback(null, header);
};

library.getTeasers = function(data, callback) {
	data.teasers.forEach(function(teaser) {
		if (teaser && teaser.content) {
			teaser.content = striptags(teaser.content, ['img']);
		}
	});
	callback(null, data);
};

library.defineWidgetAreas = function(areas, callback) {
	areas = areas.concat([
		{
			name: "Categories Sidebar",
			template: "categories.tpl",
			location: "sidebar"
		},
		{
			name: "Category Sidebar",
			template: "category.tpl",
			location: "sidebar"
		},
		{
			name: "Topic Sidebar",
			template: "topic.tpl",
			location: "sidebar"
		},
		{
			name: "Categories Header",
			template: "categories.tpl",
			location: "header"
		},
		{
			name: "Category Header",
			template: "category.tpl",
			location: "header"
		},
		{
			name: "Topic Header",
			template: "topic.tpl",
			location: "header"
		},
		{
			name: "Categories Footer",
			template: "categories.tpl",
			location: "footer"
		},
		{
			name: "Category Footer",
			template: "category.tpl",
			location: "footer"
		},
		{
			name: "Topic Footer",
			template: "topic.tpl",
			location: "footer"
		}
	]);

	callback(null, areas);
};

library.getThemeConfig = function(config, callback) {

	meta.settings.get('persona', function(err, settings) {
		config.hideSubCategories = settings.hideSubCategories === 'on';
		config.hideCategoryLastPost = settings.hideCategoryLastPost === 'on';
		config.enableQuickReply = settings.enableQuickReply === 'on';
	});

	callback(false, config);
};

function renderAdmin(req, res, next) {
	res.render('admin/plugins/persona', {});
}

library.addUserToTopic = function(data, callback) {
	if (data.req.user) {
		user.getUserData(data.req.user.uid, function(err, userdata) {
			if (err) {
				return callback(err);
			}

			data.templateData.loggedInUser = userdata;
			callback(null, data);
		});
	} else {
		callback(null, data);
	}
};

library.getLinkTags = function (data, callback) {
	data.links.push({
		rel: 'prefetch stylesheet',
		type: '',
		href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700',
	});

	callback(null, data);
};

library.getOncoColours = function () {
	var backgrounds = ['#51A9F6','#00D08A','#6053F6;','#9E64E4']; // onco theme colours
	var text = ['#fff', '#fff', '#333', '#fff', '#333', '#fff', '#fff', '#fff'];
	var index = Math.floor(Math.random() * backgrounds.length);

	return [backgrounds[index], text[index]];
};

// TODO: add a config to toggle this behaviour
library.enhanceAnonUser = function(data, callback) {
	function mapFilter(array, field) {
			return array.map(function (topic) {
					return topic && topic[field] && topic[field].toString();
			}).filter(function (value) {
					return utils.isNumber(value);
			});
	}
	var mainPids = _.uniq(mapFilter(data.topics,'mainPid'));
	async.waterfall([
			function (next) {
					// console.log("replace name waterfall start");
					posts.getPostsByPids(mainPids, data.uid, next);
			},
			function (mainPosts, next) {
					var mainPostsZip = _.zipObject(mainPids, mainPosts);
					data.topics.forEach(function (topic) {
							// if user is anonymous
							if(!parseInt(topic.uid, 10)){
									// use handle from main post as username; if exists
									if((mainPostsZip[topic.mainPid]).handle) {
											// console.log("adding username: ",(mainPostsZip[topic.mainPid]).handle);
											var stdAnonUser = JSON.parse(JSON.stringify(topic.user));
											// set handle as username
											stdAnonUser.username = (mainPostsZip[topic.mainPid]).handle;
											// set icon text as per set username
											stdAnonUser['icon:text'] = stdAnonUser.username.trim()[0];
											var colours = library.getOncoColours();
											stdAnonUser['icon:bgColor'] = colours[0];
											topic.user = stdAnonUser;
											topic.teaser.user = stdAnonUser;
									}
							}
					});
					// console.log("replace name waterfall finish");
					next(null, data);

			}
	], callback);
	
	// category data testing
	// var categories = module.parent.require('./categories');

	// var testCid = "2";
	// async.waterfall([
	// 		function (next){
	// 				async.parallel({
	// 						categoryData: function (next) {
	// 								categories.getCategoryFields(testCid, ['slug', 'disabled', 'topic_count'], next);
	// 						},
	// 						userSettings: function (next) {
	// 								user.getSettings(0, next);
	// 						},
	// 				}, next);
	// 		},
	// 		function(results,next){
	// 			var settings = results.userSettings;

	// 			var topicCount = parseInt(results.categoryData.topic_count, 10);
	// 			console.log("topicCount: ", topicCount)
	// 			var pageCount = Math.max(1, Math.ceil(topicCount / settings.topicsPerPage));
	// 			console.log("pageCount: ", pageCount);
	// 	}
	// ]);
}



module.exports = library;
