// prepare search UI for Onco-Persona style search header
// console.log("Hello World from Onco-Persona.js"); // for debugging
$('#search-fields').removeClass('hidden');
$('#search-button').addClass('hidden');

// TODO: this is not DRY code. Enable class based selectors to enable multiple instances of search on a page
var oncoPrepareSearch = function () {
    $('#search-fields-ctPage').removeClass('hidden');
    $('#search-button-ctPage').addClass('hidden');
    $('#search-fields-ctPage input').focus();
};

var oncoHandleSearch = function () {
    var searchButton = $('#search-button-ctPage');
    var searchFields = $('#search-fields-ctPage');
    var searchInput = $('#search-fields-ctPage input');

    $('#search-form-ctPage .advanced-search-link').on('mousedown', function () {
        ajaxify.go('/search');
    });

    $('#search-form').on('submit', dismissSearch);
    searchInput.on('blur', dismissSearch);

    function dismissSearch() {
        searchFields.addClass('hidden');
        searchButton.removeClass('hidden');
    }

    searchButton.on('click', function (e) {
        if (!config.loggedIn && !config.allowGuestSearching) {
            app.alert({
                message: '[[error:search-requires-login]]',
                timeout: 3000,
            });
            ajaxify.go('login');
            return false;
        }
        e.stopPropagation();

        oncoPrepareSearch();
        return false;
    });

    $('#search-form-ctPage').on('submit', function () {
        var input = $(this).find('input');
        require(['search'], function (search) {
            var data = search.getSearchPreferences();
            data.term = input.val();
            search.query(data, function () {
                input.val('');
            });
        });
        return false;
    });
};

oncoHandleSearch();
// prep the search box for view
$('#search-fields-ctPage').removeClass('hidden');
$('#search-button-ctPage').addClass('hidden');
