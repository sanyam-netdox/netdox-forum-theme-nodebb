<!-- BEGIN tags -->
<h3 class="col-lg-3 col-xs-6 tag-container">
	<a href="{config.relative_path}/tags/{tags.value}" data-value="{tags.value}">
	<div class="row">
		<span class="tag-item" data-tag="{tags.value}" style="<!-- IF tags.color -->color: {tags.color};<!-- ENDIF tags.color --><!-- IF tags.bgColor -->background-color: {tags.bgColor};<!-- ENDIF tags.bgColor -->">{tags.value}</span>
	</div>
	<div class="row tag-topic-count-row">
		<span class="tag-topic-count">Questions:</span>
		<span class="tag-topic-count human-readable-number" title="{tags.score}">{tags.score}</span>
	</div>
	</a>
</h3>
<!-- END tags -->