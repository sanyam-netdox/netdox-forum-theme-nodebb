<!-- IMPORT partials/breadcrumbs.tpl -->
<div widget-area="header">
	<!-- BEGIN widgets.header -->
	{{widgets.header.html}}
	<!-- END widgets.header -->
</div>
<div class="row">
	<div class="row">
		<div class="col-xs-12" style="margin-left:20px;">
			<span class="tag-list" style="line-height: 2.5em;">
				<span style="text-transform: uppercase;color: #777;">In Focus:</span>
				<a href="/tags/breast"><span class="tag" style="color: #000000;background-color: #e6e6e6;padding: 5px;margin:5px;font-size: 12px;text-transform: uppercase;">Breast Cancer</span></a>
				<a href="/tags/lung"><span class="tag" style="color: #000000;background-color: #e6e6e6;padding: 5px;margin:5px;font-size: 12px;text-transform: uppercase;">Lung Cancer</span></a>
				<a href="/tags/chemotherapy"><span class="tag" style="color: #000000;background-color: #e6e6e6;padding: 5px;margin:5px;font-size: 12px;text-transform: uppercase;">Chemotherapy</span></a>
				<small>•</small>
			</span>
		</div>
	</div>
	<div class="category <!-- IF widgets.sidebar.length -->col-lg-9 col-sm-12<!-- ELSE -->col-lg-12<!-- ENDIF widgets.sidebar.length -->">
		<!-- IMPORT partials/category/subcategory.tpl -->

		<!-- IF children.length --><hr class="hidden-xs"/><!-- ENDIF children.length -->

		<div class="row">
			<div class="col-md-2 col-xs-6" style="padding-bottom:10px;">
				<!-- IF privileges.topics:create -->
				<!--
				<a href="{config.relative_path}/compose?cid={cid}" component="category/post" id="new_topic" class="onco-new-question-btn btn btn-primary" data-ajaxify="false" role="button">[[category:new_topic_button]]</a>
				-->
				<a href="{config.relative_path}/compose?cid={cid}" component="category/post" id="new_topic" class="onco-new-question-btn btn btn-primary" data-ajaxify="false" role="button">POST A QUESTION</a>
				<!-- ELSE -->
					<!-- IF !loggedIn -->
					<a component="category/post/guest" href="{config.relative_path}/login" class="btn btn-primary">[[category:guest-login-post]]</a>
					<!-- ENDIF !loggedIn -->
				<!-- ENDIF privileges.topics:create -->
			</div>
			<div class="col-xs-9">
				<form id="search-form-ctPage" class="search-form navbar-form" style="margin-top:0px;padding-top:0px;padding-left:0px;" role="search" method="GET">
					<button id="search-button-ctPage" type="button" class="btn btn-link hidden"><i class="fa fa-search fa-fw" title="[[global:header.search]]"></i></button>
					<div id="search-fields-ctPage">
						<div class="form-group">
							<div class="row" style="margin-left:0px;">
								<div class="col-xs-9" style="padding-right: 0px;">
									<!--
									<input type="text" class="form-control" placeholder="[[global:search]]" name="query" value="">
									-->
									<input type="text" class="form-control" placeholder="Search by Topic" name="query" value="">
								</div>
								<div class="col-xs-1" style="margin-left: 0px;padding-left: 5px;padding-top: 10px;">
									<button type="submit" class="btn btn-default" style="padding: 0px;border: none;margin: 0px;background:#fff;">
										<i class="fa fa-search fa-fw" title="[[global:header.search]]"></i>
									</button>
								</div>
								<div class="col-xs-2" style="margin-left: 0px;padding-left: 5px;padding-top: 10px;">
									<a href="#"><i class="fa fa-gears fa-fw advanced-search-link"></i></a>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<span class="pull-right" component="category/controls">
				<!-- IMPORT partials/category/watch.tpl -->
				<!-- IMPORT partials/category/sort.tpl -->
				<!-- IMPORT partials/category/tools.tpl -->
			</span>
		</div>

		<hr class="hidden-xs" />

		<p class="hidden-xs">{name}</p>

		<!-- IF !topics.length -->
		<hr class="visible-xs" />
		<div class="alert alert-warning" id="category-no-topics">
			[[category:no_topics]]
		</div>
		<!-- ENDIF !topics.length -->

		<a href="{url}">
			<div class="alert alert-warning hide" id="new-topics-alert"></div>
		</a>

		<!-- IMPORT partials/topics_list.tpl -->

		<!-- IF config.usePagination -->
			<!-- IMPORT partials/paginator.tpl -->
		<!-- ENDIF config.usePagination -->
	</div>
	<div widget-area="sidebar" class="col-lg-3 col-sm-12 <!-- IF !widgets.sidebar.length -->hidden<!-- ENDIF !widgets.sidebar.length -->">
		<!-- BEGIN widgets.sidebar -->
		{{widgets.sidebar.html}}
		<!-- END widgets.sidebar -->
	</div>
</div>
<div widget-area="footer">
	<!-- BEGIN widgets.footer -->
	{{widgets.footer.html}}
	<!-- END widgets.footer -->
</div>

<!-- IMPORT partials/move_thread_modal.tpl -->

<!-- IF !config.usePagination -->
<noscript>
	<!-- IMPORT partials/paginator.tpl -->
</noscript>
<!-- ENDIF !config.usePagination -->
